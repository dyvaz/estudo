import { PHOTO_GET } from "../api";

const FECTH_PHOTO_STARTED = 'photo/fetchStarted';
const FECTH_PHOTO_SUCCESS = 'photo/fetchSuccess';
const FECTH_PHOTO_ERROR = 'photo/fetchError';

const fetchPhotoStarted = () => ({
  type: FECTH_PHOTO_STARTED,
})

const fetchPhotoSuccess = (data) => ({
  type: FECTH_PHOTO_SUCCESS,
  payload: data
})

const fetchPhotoError = (error) => ({
  type: FECTH_PHOTO_ERROR,
  payload: error
})

const intialState = {
  loading: false,
  data: null,
  error: null,
}

export default function photo(state = intialState, action) {
  switch (action.type) {
    case FECTH_PHOTO_STARTED:
      return {
        ...state,
        loading: true,
        data: null,
        error: null,
      };
    case FECTH_PHOTO_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload,
        error: null,
      };
    case FECTH_PHOTO_ERROR:
      return {
        ...state,
        loading: false,
        data: null,
        error: action.payload,
      };
    default:
      return state
  }
}

export const fetchPhoto = (id) => async (dispatch) => {
  try {
    dispatch(fetchPhotoStarted());
    const { url, options } = PHOTO_GET(id);
    const response = await fetch(url, options);
    const data = await response.json();
    if (response.ok === false) throw new Error(data.message);
    dispatch(fetchPhotoSuccess(data));
  } catch (error) {
    dispatch(fetchPhotoError(error.message));
  }
};