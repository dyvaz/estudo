def da_boas_vindas
  puts "Bem-vindo ao jogo da adivinhação"
  puts "Qual seu nome?"
  nome = gets.strip
  puts "\n\n\n\n\n\n"
  puts "Começaremos o jogo para você, #{nome}"
  nome
end

def pede_dificuldade
  puts "Qual o nível de dificuldade que deseja?(1 fácil , 5 difícil)"
  return dificuldade = gets.to_i
end
 
def sorteia_numero_secreto(dificuldade)
 case dificuldade
  when 1
   maximo = 30
  when 2
   maximo = 60
  when 3
   maximo = 100
  when 4
    maximo = 150
  else
    maximo = 200
  end
 puts "Escolhendo um número secreto entre 0 e #{maximo  }..."
 sorteado = rand(maximo ) +1
 puts "Escolhido... Que tal advinhar hoje nosso número secreto?"
 return sorteado
end
 
def pede_um_numero(chutes, tentativa_interna, limite_de_tentativas)
  puts"\n\n\n\n"
  puts "Tentativa #{tentativa_interna} de #{limite_de_tentativas}"
  puts "Chutes até agora: #{chutes}"
  puts "Entre com o número"
  chute = gets.strip
  puts "\n\n"
  puts "Será que você acertou? Você chutou #{chute}"
  return chute.to_i
end

def acertou(numero_secreto , chute)
  if numero_secreto == chute 
    return true
  end
  return false
end
 
def verifica_se_acertou(numero_secreto , chute)
  acertou = numero_secreto == chute 
 if acertou 
   puts "Acertou!"
   return true 
 end 
  maior = numero_secreto > chute
 if maior 
   puts "O número secreto é maior!"
   puts "o numero era #{numero_secreto}"
  else 
   puts "O número secreto é menor!" 
   puts "o numero era #{numero_secreto}"
 end 
  return false

end


def por_pouco(numero_secreto , chute)
  return chute == numero_secreto - 1 || chute == numero_secreto + 1
end

def perdeu

  puts "  \\ | /  ______  \\ | /    "
  puts "    @~ /  ,  .  \\ ~@      "
  puts "   /__(  \\____/  )__\\      "
  puts "       \\_____U__/           "
  puts " Você perdeu"

end

$ganhei = 0
$perdi = 0
$joguei = 0 


def joga(nome, dificuldade )

  numero_secreto = sorteia_numero_secreto dificuldade
  limite_de_tentativas = 5
  chutes = []
  pontos_ate_agora = 1000
  tentativa_extra = false


  for tentativa in 1..limite_de_tentativas
    chute = pede_um_numero chutes, tentativa , limite_de_tentativas

    if chutes.include?(chute)
      puts "Você ja usou este número"
      puts "tente outro"
      redo  
    end
       chutes << chute

    if por_pouco numero_secreto, chute and tentativa == 5 and tentativa_extra == false
      tentativa_extra = true
      puts "errou por 1"
      redo 
    end


    pontos_a_perder = (chute - numero_secreto).abs/2.0
    pontos_ate_agora = pontos_ate_agora - pontos_a_perder

    if verifica_se_acertou numero_secreto , chute 
      $ganhei = $ganhei + 1
      break
    end
   
  end 

  unless acertou(numero_secreto , chute)
   perdeu
   $perdi = $perdi + 1
  end
  $joguei += 1
 puts "\n\n"
 puts "Você ganhou #{pontos_ate_agora} pontos"
 puts " voce ganhou #{$ganhei} vezes"
 puts "Voce perdeu #{$perdi} vezes"

end
 
def nao_quer_jogar?
   puts "sua porcentagem de vitoria é de #{($ganhei/ $joguei )* 100}% "
   puts "Deseja jogar novamente? (S/N)"
   quer_jogar = gets.strip
   nao_quero_jogar = quer_jogar.upcase == "N"
end

nome = da_boas_vindas
dificuldade = pede_dificuldade
nivel_dificuldade =nivel_dificuldade




loop do
   joga nome, dificuldade
   break if nao_quer_jogar?
end
