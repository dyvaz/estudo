def da_boas_vindas
  puts "/****************/"
  puts "Jogo de forca"
  puts "/****************/"
  puts "Qual é o seu nome?"
  nome = gets.strip
  puts "\n\n\n\n\n\n"
  puts "Começaremos o jogo para você, #{nome}"
  return nome
end

def desenha_forca(erros)
  cabeca = "   "
  corpo = " "
  pernas = "   "
  bracos = "   "
  if erros >= 1
    cabeca = "(_)"
  end
  if erros >= 2
    bracos = "\\|/"
  end
  if erros >= 3
    corpo = " | "
  end
  if erros >= 4
    pernas = "/ \\"
  end

  puts "  _______       "
  puts " | /     |      "
  puts " |      #{cabeca}   "
  puts " |      #{bracos}   "
  puts " |      #{corpo}      "
  puts " |      #{pernas}    "
  puts " |              "
  puts "_|___           "
  puts ""
end

  

def cabecalho_de_tentativa(chutes, erros, mascara,acertos)
  puts "\n\n\n\n"
  puts "Palavra secreta: #{mascara}" 
  desenha_forca erros
  puts "Erros ate agora: #{erros}"
  puts "Acertos ate agora: #{acertos}"
  puts "Chutes ate agora: #{chutes}"
end

def avisa_acertou_palavra
  puts "Parbéns, você ganhou!"

  puts "       ___________       "
  puts "      '._==_==_=_.'      "
  puts "      .-\\:      /-.     "
  puts "     | (|:.     |) |     "
  puts "      '-|:.     |-'      "
  puts "        \\::.    /       "
  puts "         '::. .'        "
  puts "          )   (         "
  puts "        _.'   '._        "
  puts "       '---------'      "
end


def pede_um_chute
  puts "Entre com a letra ou palavra"
  chute = gets.strip.downcase
  puts "Será que acertou? Você chutou '#{chute}' "
  return chute
end




def nao_quer_jogar?
  puts "Deseja jogar novamente? (S/N)"
  quero_jogar = gets.strip
  nao_quero_jogar = quero_jogar.upcase == "N"
  return nao_quero_jogar
end

def avisa_escolhendo_palavra
  puts "Escolhendo uma palavra..."
end

def avisa_palavra_escolhida(palavra_secreta)
  puts "Escolhida uma palavra com #{palavra_secreta.size} caracteres... Boa sorte!"
  return palavra_secreta
end



def avisa_chute_repetido(chute)
  puts "voce ja chutou #{chute}"
end

def avisa_letra_nao_encontrada
  puts "Letra nao encontrada"
end

def avisa_letra_encontrada(total_encontrado)
  puts "Letra encontrada #{total_encontrado} vezes"
end


def avisa_errou_palavra
  puts "Que pena... errou"
end

def avisa_pontos(pontos_ate_agora)
  puts "voce ganhou #{pontos_ate_agora} pontos"
end 

def avisa_pontos_totais(pontos)
  puts "Voce possui #{pontos} pontos"
end


def avisa_campeao_atual(rank)
  puts "Nosso campeão atual é #{rank[0]} com #{rank[1]} pontos."
end

def ver_campeao_atual(nome,string_rank)
  if nome != string_rank[0] 
    puts "Parabéns,voce é o atual campeão"
  end
end

def perdeu palavra_secreta
  puts " Poxa, você perdeu"
  puts "  A palavra era #{palavra_secreta}"

  puts "      _______________         "
  puts "    /                 \\      "
  puts "   /                   \\     "
  puts " //                     \\/\\ " 
  puts " \\|  XXXX      XXXX     | /  "
  puts "  |  XXXX      XXXX     |/    "
  puts "  |  XXX        XXX     |     "
  puts "  |                     |     "
  puts "  \\__      XXX        __/    "
  puts "    |\\     XXX       /|      "  
  puts "    | |             | |       "
  puts "    |  I I I I I I I  |       "
  puts "    |   I I I I I I   |       "
  puts "    \\_               _/      "
  puts "      \\_          _/         "
  puts "        \\________/           "
end