require_relative 'ui'
require_relative 'rank'
require_relative 'recorde'

def sorteia_palavra_secreta 
  avisa_escolhendo_palavra
  
  texto = File.read("dicionario.txt")
  todas_as_palavras = texto.split("\n")
  loop do
    numero_aleatorio = rand(todas_as_palavras.size)
    palavra_secreta = todas_as_palavras[numero_aleatorio].downcase 
    if palavra_secreta == ""
      next
    else 
      return palavra_secreta.strip
    end

  end

end


def pede_um_chute_valido(chutes, erros,  mascara, acertos)
  cabecalho_de_tentativa chutes, erros, mascara, acertos
  loop do
    chute = pede_um_chute 
    if chutes.include? chute
      avisa_chute_repetido chute
      next
    else
      return chute
    end
  end
end

def joga(nome)
  palavra_secreta = sorteia_palavra_secreta 
  avisa_palavra_escolhida palavra_secreta
 
  erros = 0
  acertos = 0
  chutes = []
  pontos_ate_agora = 0

  while erros < 5
    mascara = palavra_mascarada chutes, palavra_secreta
    chute = pede_um_chute_valido chutes, erros, mascara, acertos
    chutes << chute

    chutou_uma_unica_letra = chute.size == 1
    if chutou_uma_unica_letra
      total_encontrado = palavra_secreta.count(chute[0]) 
      if total_encontrado == 0
        avisa_letra_nao_encontrada
        erros +=1
        pontos_ate_agora -=30
      else 
        acertos +=1
        avisa_letra_encontrada total_encontrado
      end
    else
      acertou = chute == palavra_secreta
      if acertou
        avisa_acertou_palavra
        pontos_ate_agora += 100
        break
      else
        avisa_errou_palavra
        pontos_ate_agora -=30
        erros +=1
      end
    end
    if erros == 5
      perdeu palavra_secreta
    end
  end

  avisa_pontos pontos_ate_agora

  return pontos_ate_agora

end

def is_number?(letra)
  letra.to_i.to_s == letra
end


def palavra_mascarada(chutes, palavra_secreta)
   mascara = ""
   for letra in palavra_secreta.chars
    e_numero = is_number? letra

    # puts e_numero
    # puts letra.to_i
    # puts letra.to_s
    # puts letra.to_i.to_s

    if !chutes.include? letra and letra != " " and letra != "-" and !e_numero
      letra = "_"
    end
    mascara += letra

  end 
  return mascara
end





def jogo_da_forca
  nome = da_boas_vindas
  pontos_totais = 0

  rank_file = "rank.txt"
  jogadores_file = "jogadores.txt"

  conteudo_rank = File.read(rank_file)

  string_rank = string_rank conteudo_rank


  if conteudo_rank != ""
    avisa_campeao_atual string_rank
  end



  loop do 
    pontos_totais += joga nome
    avisa_pontos_totais pontos_totais

    lista = File.read(jogadores_file)
    jogadores = string_para_recorde lista

    if !jogadores.include?(nome) || jogadores[nome] < pontos_totais
      puts "Parabéns, voce bateu seu próprio recorde"
      jogadores[nome] = pontos_totais
      lista_str = recorde_para_string jogadores
      File.write jogadores_file, lista_str
    end


    if string_rank[1].to_i < pontos_totais
      novo_rank = novo_rank nome,pontos_totais
      ver_campeao_atual nome, string_rank
      conteudo =  rank_string novo_rank
      File.write rank_file, conteudo 
    end

    break if nao_quer_jogar? 

  end
  return pontos_totais
end
