const comidas = ['Pizza', 'Frango', 'Carne', 'Macarrao'];

const primeiroValor = comidas.shift();
const ultimoValor = comidas.pop();

comidas.push('arroz');
comidas.unshift('peixe');

console.log(primeiroValor);
console.log(ultimoValor);
console.log(comidas);

const estudantes = ['Marcio', 'Brenda', 'Joana', 'Kleber', 'Julia'];

estudantes.sort();
estudantes.reverse();

console.log(estudantes);
console.log(estudantes.includes('Joana'));

let html = `<section>
               <div>Sobre</div>
               <div>Produtos</div>
               <div>Contatos</div>
            </section`;

html = html.split('section').join('ul').split('div').join('li');

console.log(html);

const carros = ['Ford', 'fiat', 'VW', 'Honda'];

const carrosCopia = carros.slice();
carros.pop();

console.log(carros);
